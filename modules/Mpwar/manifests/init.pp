class Mpwar{
	class { 'apache':}

	include '::yum'

	apache::vhost { 'myMpwar.dev':
	port => '80',
	docroot => '/web',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
	}

	include '::mysql::server'

	mysql::db { 'mympwar':
	  user     => 'root',
	  password => 'katiusca',
	  host     => 'localhost',
	}

	# PHP
	$php_version = '55'

	# remi_php55 requires the remi repo as well
	if $php_version == '55' {
	  $yum_repo = 'remi-php55'
	  include ::yum::repo::remi_php55
	}
	# remi_php56 requires the remi repo as well
	elsif $php_version == '56' {
	  $yum_repo = 'remi-php56'
	  ::yum::managed_yumrepo { 'remi-php56':
		descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',
		mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',
		enabled        => 1,
		gpgcheck       => 1,
		gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
		gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
		priority       => 1,
	  }
	}
	# version 5.4
	elsif $php_version == '54' {
	  $yum_repo = 'remi'
	  include ::yum::repo::remi
	}

	class { 'php':
	  version => 'latest',
	  require => Yumrepo[$yum_repo]
	}
	
	class { 'memcached':}

	host { 'localhost':
	ensure => 'present',
	target => '/etc/hosts',
	ip => '127.0.0.1',
	host_aliases => ['myMpwar.prod']
	}
}

